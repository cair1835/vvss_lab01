package lab04;

import biblioteca.model.Carte;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;

public class Test_Lab4 {
    private static CartiRepoMock cartiRepoMock;

    @BeforeClass
    public static void  initialize(){
        cartiRepoMock = new CartiRepoMock();
    }

    @Test
    public void Test1(){
        int anAparitie = 1973;
        List<Carte> result = cartiRepoMock.getCartiOrdonateDinAnul(String.valueOf(anAparitie));
        Assert.assertEquals(2, result.size());
        Assert.assertEquals("Poezii", result.get(0).getTitlu());
    }

    @Test
    public void Test2(){
        List<Carte> result = cartiRepoMock.getCartiOrdonateDinAnul("0"); // error
        Assert.assertEquals(2, result.size());
        Assert.assertEquals("Poezii", result.get(0).getTitlu());

        // fix
//        List<Carte> result = cartiRepoMock.getCartiOrdonateDinAnul("0");
//        Assert.assertEquals(0, result.size());
    }
}
