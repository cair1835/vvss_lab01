package lab03;

import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class BBT {
    // TC1_ECP
    @Test
    public void Test1() throws Exception {
        BibliotecaCtrl controller = new BibliotecaCtrl(new CartiRepoMock());
        Carte c = new Carte();
        c.setTitlu("titlu");
        c.setReferenti(new ArrayList<String>() {{
            add("autor");
            add("autor");
        }});
        c.setAnAparitie(1232);
        c.setEditura("editura");
        c.setCuvinteCheie(new ArrayList<String>() {
            {
                add("keyword");
                add("keywordd");
            }});
        controller.adaugaCarte(c);
        assertEquals(controller.getCarti().size(),7);
    }

    // TC2_ECP
    @Test
    public void Test2() throws Exception {
        BibliotecaCtrl controller = new BibliotecaCtrl(new CartiRepoMock());
        Carte c = new Carte();
        c.setTitlu("");
        c.setReferenti(new ArrayList<String>() {{
            add("autor");
            add("autor");
        }});
        c.setAnAparitie(1232);
        c.setEditura("editura");
        c.setCuvinteCheie(new ArrayList<String>() {
            {
                add("keyword");
                add("keywordd");
            }});
        controller.adaugaCarte(c);
        assertEquals(controller.getCarti().size(),7);
    }

    // TC3_ECP
    @Test
    public void Test3() throws Exception {
        BibliotecaCtrl controller = new BibliotecaCtrl(new CartiRepoMock());
        Carte c = new Carte();
        c.setTitlu("titlu");
        c.setAnAparitie(1232);
        c.setEditura("editura");
        c.setCuvinteCheie(new ArrayList<String>() {
            {
                add("keyword");
                add("keywordd");
            }});
        controller.adaugaCarte(c);
        assertEquals(controller.getCarti().size(),7);
    }

    // TC4_ECP
//    @Test
//    public void Test4() throws Exception {
//        BibliotecaCtrl controller = new BibliotecaCtrl(new CartiRepoMock());
//        Carte c = new Carte();
//        c.setTitlu("titlu");
//        c.setReferenti(new ArrayList<String>() {{
//            add("autor");
//            add("autor");
//        }});
//        c.setAnAparitie("1232");
//        c.setEditura("editura");
//        c.setCuvinteCheie(new ArrayList<String>() {
//            {
//                add("keyword");
//                add("keywordd");
//            }});
//        controller.adaugaCarte(c);
//        assertEquals(controller.getCarti().size(),7);
//    }

    // TC5_ECP
    @Test
    public void Test5() throws Exception {
        BibliotecaCtrl controller = new BibliotecaCtrl(new CartiRepoMock());
        Carte c = new Carte();
        c.setTitlu("titlu");
        c.setReferenti(new ArrayList<String>() {{
            add("autor");
            add("autor");
        }});
        c.setAnAparitie(2010);
        c.setEditura("");
        c.setCuvinteCheie(new ArrayList<String>() {
            {
                add("keyword");
                add("keywordd");
            }});
        controller.adaugaCarte(c);
        assertEquals(controller.getCarti().size(),7);
    }

    // TC6_ECP
    @Test
    public void Test6() throws Exception {
        BibliotecaCtrl controller = new BibliotecaCtrl(new CartiRepoMock());
        Carte c = new Carte();
        c.setTitlu("titlu");
        c.setReferenti(new ArrayList<String>() {{
            add("autor");
            add("autor");
        }});
        c.setAnAparitie(1232);
        c.setEditura("editura");
        controller.adaugaCarte(c);
        assertEquals(controller.getCarti().size(),7);
    }

    // TC20_BVA
    @Test
    public void Test7() throws Exception {
        BibliotecaCtrl controller = new BibliotecaCtrl(new CartiRepoMock());
        Carte c = new Carte();
        String titlu = "";
        for(int i = 0; i < 256; i++){
            titlu += "a";
        }
        c.setTitlu(titlu);
        c.setReferenti(new ArrayList<String>() {{
            add("autor");
            add("autor");
        }});
        c.setAnAparitie(1232);
        c.setEditura("editura");
        c.setCuvinteCheie(new ArrayList<String>() {
            {
                add("keyword");
                add("keywordd");
            }});
        controller.adaugaCarte(c);
        assertEquals(controller.getCarti().size(),7);
    }

    //  TC22_BVA
    @Test
    public void Test8() throws Exception {
        BibliotecaCtrl controller = new BibliotecaCtrl(new CartiRepoMock());
        Carte c = new Carte();
        String titlu = "";
        for(int i = 0; i <= 256; i++){
            titlu += "a";
        }
        c.setTitlu(titlu);
        c.setReferenti(new ArrayList<String>() {{
            add("autor");
            add("autor");
        }});
        c.setAnAparitie(1232);
        c.setEditura("editura");
        c.setCuvinteCheie(new ArrayList<String>() {
            {
                add("keyword");
                add("keywordd");
            }});
        controller.adaugaCarte(c);
        assertEquals(controller.getCarti().size(),7);
    }

}
