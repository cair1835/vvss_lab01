package lab02;

import biblioteca.model.Carte;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class WBT {

    // TC1
    @Test
    public void Test1_WBT() throws Exception {
        CartiRepoMock cartiRepoMock = new CartiRepoMock(1);
        List<Carte> cartiGasite = cartiRepoMock.cautaCarte("adrian");
        assertEquals(0, cartiGasite.size());
    }

    // TC2
    @Test
    public void Test2_WBT() throws Exception {
        CartiRepoMock cartiRepoMock = new CartiRepoMock();
        List<Carte> cartiGasite = cartiRepoMock.cautaCarte("mihai");
        assertEquals(1, cartiGasite.size());
    }

    // TC3 and TC4
    @Test
    public void Test3_WBT() throws Exception {
        CartiRepoMock cartiRepoMock = new CartiRepoMock();
        List<Carte> cartiGasite = cartiRepoMock.cautaCarte("tragedie");
        assertEquals(0, cartiGasite.size());
    }

}
