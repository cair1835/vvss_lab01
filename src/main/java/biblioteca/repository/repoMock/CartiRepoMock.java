package biblioteca.repository.repoMock;

import biblioteca.model.Carte;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CartiRepoMock implements CartiRepoInterface {

        private List<Carte> carti;

        public CartiRepoMock(){
            carti = new ArrayList<Carte>();

            carti.add(Carte.getCarteFromString("Povesti;Mihai Eminescu,Ion Caragiale,Ion Creanga;1973;Corint;povesti,povestiri"));
            carti.add(Carte.getCarteFromString("Poezii;Sadoveanu;1973;Corint;poezii"));
            carti.add(Carte.getCarteFromString("Enigma Otiliei;George Calinescu;1948;Litera;enigma,otilia"));
            carti.add(Carte.getCarteFromString("Dale carnavalului;Caragiale Ion;1948;Litera;caragiale,carnaval"));
            carti.add(Carte.getCarteFromString("Intampinarea crailor;Mateiu Caragiale;1948;Litera;mateiu,crailor"));
            carti.add(Carte.getCarteFromString("Test;Calinescu,Tetica;1992;Pipa;am,casa"));
        }

        public CartiRepoMock(int index){
            carti = new ArrayList<Carte>();

        }

        @Override
        public void adaugaCarte(Carte c) {
            carti.add(c);
        }

        @Override
        public List<Carte> cautaCarte(String ref) {
            List<Carte> cartiGasite = new ArrayList<Carte>();
            List<Carte> carti = getCarti();

            int i=0;
            while (i<carti.size()){
                boolean flag = false;
                List<String> lref = carti.get(i).getAutori();
                int j = 0;
                while(j<lref.size()){
                    if(lref.get(j).toLowerCase().contains(ref.toLowerCase())){
                        cartiGasite.add(carti.get(i));
                    }
                    j++;
                }
                i++;
            }
            return cartiGasite;
        }

        @Override
        public List<Carte> getCarti() {
            return carti;
        }

        @Override
        public void modificaCarte(Carte nou, Carte vechi) {
            int index = carti.indexOf(vechi);
            carti.set(index, nou);
        }

        @Override
        public void stergeCarte(Carte c) {
            int index = carti.indexOf(c);
            carti.remove(index);
        }

        @Override
        public List<Carte> getCartiOrdonateDinAnul(String an) {
            List<Carte> lc = getCarti();
            List<Carte> lca = new ArrayList<Carte>();

            int anAparitie = Integer.parseInt(an);

            for(Carte c:lc){
                if(c.getAnAparitie() == anAparitie){
                    lca.add(c);
                }
            }

            Collections.sort(lca,new Comparator<Carte>(){

                @Override
                public int compare(Carte a, Carte b) {
                    if(a.getTitlu().compareTo(b.getTitlu())==0){
                        return a.getAutori().get(0).compareTo(b.getAutori().get(0));
                    }

                    return a.getTitlu().compareTo(b.getTitlu());
                }

            });

            return lca;
        }

        @Override
        public Carte getCarteDupaTitlu(String titlu) {
            for(Carte c: getCarti()){
                if(c.getTitlu().toLowerCase().equals(titlu.toLowerCase())){
                    return c;
                }
            }
            return null;
        }

}
